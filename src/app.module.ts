import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NewsController } from './news/news.controller';
import { UsersController } from './users/users.controller';
import { UsersService } from './users/users.service';
import { UsersModule } from './users/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/ormconfig';
import { NewsModule } from './news/news.module';

@Module({
  //importa modulos
  imports: [TypeOrmModule.forRoot(typeOrmConfig), UsersModule, NewsModule],
  //importa controladores
  controllers: [AppController, NewsController, UsersController],
  //importa el servicio
  providers: [AppService, UsersService],
})
export class AppModule {}
