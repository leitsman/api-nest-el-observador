import { Injectable } from '@nestjs/common';
import { CreateNewsDto } from './dto/create-news.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { News } from './entities/news.entity';
import { Repository } from 'typeorm';
// import { UpdateNewsDto } from './dto/update-news.dto';

@Injectable()
export class NewsService {
  constructor(
    @InjectRepository(News)
    private readonly newsRepository: Repository<News>,
  ) {}

  async create(createNewsDto: CreateNewsDto) {
    try {
      return await this.newsRepository.save(createNewsDto);
    } catch (error) {
      console.log(error);
    }
  }

  async findAll() {
    try {
      return this.newsRepository.find();
    } catch (error) {
      console.log(error);
    }
  }

  // findOne(id: number) {
  //   return `This action returns a #${id} news`;
  // }
  //updateNewsDto: UpdateNewsDto -> parameters in update
  // update(id: number) {
  //   return `This action updates a #${id} news`;
  // }

  async remove(id: number) {
    try {
      const elementNews = await this.newsRepository.findOneBy({ id });
      if (!elementNews) throw new Error('news not found');
      await this.newsRepository.delete(id);
      return elementNews;
    } catch (error) {
      console.log(error);
    }
  }
}
