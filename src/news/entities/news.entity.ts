import { BaseEntity } from 'src/common/baseEntity';
import { Column, Entity } from 'typeorm';

@Entity()
export class News extends BaseEntity {
  @Column({ nullable: false })
  title: string;

  @Column({ nullable: false })
  date: string;

  @Column({ nullable: false })
  place: string;

  @Column({ nullable: false })
  autor: string;

  @Column({ nullable: false })
  content: string;
}
