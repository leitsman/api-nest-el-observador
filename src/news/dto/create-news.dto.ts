import { IsNotEmpty, IsString, MinLength } from 'class-validator';

export class CreateNewsDto {
  @IsString()
  @MinLength(5)
  @IsNotEmpty()
  title: string;

  @IsString()
  @MinLength(5)
  @IsNotEmpty()
  date: string;

  @IsString()
  @MinLength(5)
  @IsNotEmpty()
  place: string;

  @IsString()
  @MinLength(5)
  @IsNotEmpty()
  autor: string;

  @IsString()
  @MinLength(10)
  @IsNotEmpty()
  content: string;
}
