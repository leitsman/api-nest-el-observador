import { Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  // @Column({
  //   default: false,
  // })
  // is_deleted: boolean;

  // @DeleteDateColumn({
  //   name: 'deleted_at',
  //   type: 'timestamp',
  //   default: null,
  // })
  // deletedAt: Date;
}
