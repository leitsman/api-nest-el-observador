import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Post,
  Query,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
//todo se encarga de las rutas tambien de las peticiones http: body auth
@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}
  @Post()
  @HttpCode(201)
  create(@Body() user: CreateUserDto) {
    //? desde aqui recibimos el cuerpo json y le enviamos a service a procesar
    return this.usersService.create(user);
  }

  @Get()
  //@Req() request: Request, @Res() response: Response
  findAll(@Query() query: any) {
    //? desde aqui controlamos query, res, req
    console.log(query);

    return this.usersService.findAll();
  }

  @Delete(':id')
  remove(): string {
    return this.usersService.remove();
  }
}
