import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import { join } from 'path';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useStaticAssets(join(__dirname, '..', 'public'));
  app.setBaseViewsDir(join(__dirname, '..', 'views'));
  app.setViewEngine('hbs');
  //añadir globalPipes para validacion de clases
  //? la linea de pipes hara la validacion en cada dto
  app.useGlobalPipes(new ValidationPipe({ whitelist: true }));

  await app.listen(4070);
}
bootstrap();
