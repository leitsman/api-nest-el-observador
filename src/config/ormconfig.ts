import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'root',
  database: 'el_observador_db',
  entities: [__dirname + '/../**/*.entity{.ts,.js}'],
  synchronize: true,
  logging: true,
};

// import { ConfigModule, ConfigService } from '@nestjs/config';
// import {
//   TypeOrmModuleAsyncOptions,
//   TypeOrmModuleOptions,
// } from '@nestjs/typeorm';

// export default class TypeOrmConfig {
//   static getOrmConfig(configService: ConfigService): TypeOrmModuleOptions {
//     console.log(configService.get('DB_DATABASE'));
//     console.log(configService.get('DB_HOST'));
//     console.log(configService.get('DB_PORT'));
//     console.log(configService.get('DB_USER'));
//     console.log(configService.get('DB_SYNCHRONIZE'));
//     return {
//       type: 'postgres',
//       host: configService.get('DB_HOST'),
//       port: configService.get('DB_PORT'),
//       username: configService.get('DB_USER'),
//       password: configService.get('DB_PASSWORD'),
//       database: configService.get('DB_DATABASE'),
//       entities: [__dirname + '/../**/*.entity{.ts,.js}'],
//       synchronize: configService.get('DB_SYNCHRONIZE'),
//       logging: configService.get('DB_LOGGING'),
//     };
//   }
// }

// export const typeOrmConfigAsync: TypeOrmModuleAsyncOptions = {
//   imports: [ConfigModule],
//   useFactory: async (
//     configureService: ConfigService,
//   ): Promise<TypeOrmModuleOptions> =>
//     TypeOrmConfig.getOrmConfig(configureService),
//   inject: [ConfigService],
// };
