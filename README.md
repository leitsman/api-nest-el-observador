## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Conectar su DB al proyecto con typeOrm y postgreSql
1. Dirigase al archivo ubicado en: src/config/ormconfig.ts

2. Intercambie los 'INSERT' y 0000 por sus respectivos valores
```typescript
export const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'INSERT',
  host: 'INSERT',
  port: 0000,
  username: 'INSERT',
  password: 'INSERT',
  database: 'INSERT',
  entities: [__dirname + '/../**/*.entity{.ts,.js}'],
  synchronize: true,
  logging: true,
};
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).